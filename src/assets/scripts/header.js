$(()=>{
	var width = $(window).innerWidth();
	if (width < 991) {
		$('.navbar-nav').appendTo('.menu-responsive');
		$('.nav-bar-socials').appendTo('.menu-responsive');
	}
	$('.navbar-toggler').click(function() {
		$('.nav-responsive').addClass('active');
		$('.bg-nav').addClass('active');
		$('body').addClass('active');
	});
	$('.nav-responsive .icon-close').click(function() {
		$('.nav-responsive').removeClass('active');
		$('.bg-nav').removeClass('active');
		$('body').removeClass('active');
	});
	$('.bg-nav').click(function() {
		$('.nav-responsive').removeClass('active');
		$('.bg-nav').removeClass('active');
		$('body').removeClass('active');
	});
});