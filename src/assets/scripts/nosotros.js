/* requires:
	jquery.bxslider.min.js
	lightgallery.js
	lg-fullscreen.js
	lg-thumbnail.js
	lg-zoom.js
	lg-video.js
	lg-autoplay.js
*/
$(()=>{
	$('.videoGallery').lightGallery({
		selector:'a'
	});	
	$('.slider-nosotros').bxSlider({
	  mode: 'fade',
	  controls: true,
	  pager: false,
	  auto: true
	});
})