/* requires:
	jquery.validationEngine.js
	jquery.validationEngine-es.js
*/
$(function(){
	//bloque23
	$( ".b2-itm input, .b2-itm textarea" ).focusin(function() {
		$(this).parent().addClass('active');
	});

	$( ".b2-itm input, .b2-itm textarea" ).focusout(function() {
		if ($(this).val() === "") {
			$(this).parent().removeClass('active');
		};
	});
	$( ".b2-itm input, .b2-itm textarea" ).each(function() {
		if ($(this).val() != "") {
			$(this).parent().addClass('active');
		};
	});

	$("form").validationEngine('attach', {
		promptPosition : "topLeft",
		autoHidePrompt: true,
		autoHideDelay: 3000,
		binded: false,
		scroll: false,
		validateNonVisibleFields: true
	});

	$("#form-contacto").submit(function(e) {
		var valid = $(this).validationEngine('validate');
		if (!valid){
			return false;
		}else{
			return true;
		};
	});

	
});