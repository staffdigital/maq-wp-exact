/* requires:
	jquery.bxslider.min.js
	slick.min.js
	lightgallery.js
	lg-fullscreen.js
	lg-thumbnail.js
	lg-zoom.js
	lg-video.js
	lg-autoplay.js
*/
$(function(){
	function clone_svg(){
		$('.svgloader').clone().appendTo('.homeslider .bx-pager-link').removeClass('desktop');
	}
	$('.bxsliderHome').bxSlider({
		mode: 'fade',
		auto:false,
		pause: 6000,
		adaptiveHeight:true,
		pager:true,
		controls:false,
		onSliderLoad: function(currentIndex) {
			$('.homeitm').children().eq(currentIndex).addClass('active')
			clone_svg();
		},
	});

	// fancy imagen
	$('.videoGallery').lightGallery({
		selector:'a'
	});		
	
	var logos_item = $('.homeslidlog ul li').length;
	if (logos_item > 6) {
		$('.homeslidlog').removeClass('no-slide');
		$('.pitcher-carousel').slick({
			infinite: true,
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 1000,
			speed: 500,
			responsive:[
				{
					breakpoint: 1025,
					settings: {
						slidesToShow: 5
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 481,
					settings: {
						slidesToShow: 3
					}
				}
			]
		});			
	} else {
		$('.homeslidlog').addClass('no-slide');			
	}




	$(window).resize(function(event) {
		clone_svg();
	});

	// nosotros
		$('.nosotros-carousel').slick({
			infinite: true,
			slidesToShow: 6,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 1000,
			speed: 500,
			responsive:[
				{
					breakpoint: 1025,
					settings: {
						slidesToShow: 5
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 481,
					settings: {
						slidesToShow: 3
					}
				}
			]
		});			
});