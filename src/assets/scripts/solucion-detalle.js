/* requires:
	jquery.waypoints.js
	slick.min.js
*/
$(()=>{
	$('#exampleModal').on('shown.bs.modal', function () {
	})
	var solucionAnima = $('.solution-ben');
	solucionAnima.waypoint(function(direction) {
		if (direction === 'down') {
			$('.solution-ben').addClass('active');
		}
		}, {
			offset:'65%'
	});
	$('.solution-ctn-slider ul').slick({
		arrows: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 1000,
		speed: 500,
		responsive:[
			{
				breakpoint: 1025,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 481,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});	
	$('.btn-arrow-prev').click(function(){
	    $('.solution-ctn-slider ul').slick('slickPrev');
	});
	$('.btn-arrow-next').click(function(){
	    $('.solution-ctn-slider ul').slick('slickNext');
	});
})